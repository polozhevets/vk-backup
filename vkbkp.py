#!/usr/bin/python3
# -*- coding: UTF-8 -*-
#python3.5
#vk_api v5.52

import vk
import time
import json
import argparse
import getpass
import sys
import os

app_id = '3714607'
folder = 'conversations' #название папки в которую сохранять историю диалогов
user_login = input("Email: ")
user_password = getpass.getpass()

session = vk.AuthSession(app_id=app_id, user_login=user_login, user_password=user_password, scope='friend,messages')
api = vk.API(session)

currenttime = time.time()

def createParser ():
    parser = argparse.ArgumentParser()
    parser.add_argument ('-u', '--id', default='0')
    return parser

#получить список друзей
def get_friends():
    friends_list = api.friends.get(order='hints', count='20', fields='first_name,last_name')
    return friends_list

#получить инфу о юзере
def get_user_info(user_id):
    data = api.users.get(user_ids=user_id)
    first_name = data[0]['first_name']
    last_name = data[0]['last_name']
    return {'first_name': first_name, 'last_name': last_name}
    

#получить историю переписки с юзером
def get_im_hystory(user_id):
    maxcountim = 200 #максимальное количество запросов за одно обращение к API VK
    offset = 0 #смещение, необходимое для выборки определенного подмножества сообщений
    imhistory = []
    cycletime = time.time()
    stop = 0
    while stop == 0:
        cycle = api.messages.getHistory(offset=str(offset), user_id=user_id, count=str(maxcountim), rev=1)
        count = cycle[0]
        print ('offset:', offset,'total:', count)
        items = cycle[1:]
        for item in items:
            imhistory.append(item)
            if len(imhistory)==count:
                stop = 1
        offset = offset+maxcountim
        time.sleep(0.5)

    result = imhistory
    return result


#сохранить историю переписки в json файл
def save_json(data, filename):
    with open(filename, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False)

#основной код
if __name__ == '__main__':
    parser = createParser()
    namespace = parser.parse_args(sys.argv[1:])

if namespace.id != '0':
    user_id = namespace.id

if namespace.id == '0':
    n=1
    print ('\nYour friends list:\n')
    friends_list = get_friends()
    for friend in friends_list:
        print (n,':', friend['first_name'], friend['last_name'])
        n=n+1
    friend_number = input('\nPlese enter friend number: ')
    selected = friends_list[int(friend_number)-1]
    user_id = selected['user_id']

user_info = get_user_info(user_id)
print ('\nSelected:', user_info['first_name'], user_info['last_name'])

user_history = get_im_hystory(user_id)
print ('results:', len(user_history))
filename = user_info['first_name']+'_'+user_info['last_name']+'_'+time.strftime('%b%d%Y_%H%M%S%Z')+'.json'
if not os.path.exists(folder):
    os.makedirs(folder)
save_json(user_history, folder+'/'+filename)