window.onload = function() {
		var fileInput = document.getElementById('fileInput');
		var fileDisplayArea = document.getElementById('fileDisplayArea');

		fileInput.addEventListener('change', function(e) {
			var file = fileInput.files[0];
			var textType = /text.*/;
			var reader = new FileReader();

			reader.onload = function(e) {
				var jsondata = JSON.parse(reader.result)
				var chat = ""
				for (var i=0; i<jsondata.length; i++) {
					if (jsondata[i].out=="1") {
						var color="#4078c0";
					}
					if (jsondata[i].out=="0") {
						var color="#333";
					}

					chat = chat+"<p style='color:"+color+";'>"+(jsondata[i].body)+"</p>";
				}
				fileDisplayArea.innerHTML = chat;
			}
			reader.readAsText(file);	

		});
}
